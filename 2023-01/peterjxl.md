# PeterJXL

老师和同学们好，我是晓林。

还记得第一次学习Git的时候是在大学，当时看到廖雪峰老师的教程，惊为天人！有理论、有实践、有广度，不仅学起来容易，也基本上够我们在日常工作中使用了。

如今我已工作两年，对于Git的使用还是比较简单，没有去深入；目前接触到了一个大型的项目，有十几个人维护，数十个分支，成百上千个commit，版本管理起来很复杂；而我已经对Git的不少概念都记不清了，这应该是因为当时只学了下，并且只练习了下Git命令，但没有在实际生活中用起来；

为此，我打算重新复习，第一个想到的就是廖雪峰老师的教程，因此重新花了一周来学习，并且输出了十几篇笔记（为避免广告的嫌疑，这里就不放链接了），并且在工作中实际运用起来

最后感谢老师的 git 教程，并且给我们发起 Pull Request 的机会。

下面是我自己小结的Git命令，有需自取。

## Git版本

```bash
# 查看Git版本
$ git --version 
```

　　‍

## 创建版本库

```bash
# 在当前目录创建版本库
$ git init   

# 新建一个demo文件夹，并在里面创建版本库
$ git init demo

# 下载一个项目
$ git clone [url]

# 查看版本库状态
$ git status 
```

　　‍

## 配置Git 

　　Git的配置有项目配置 -> 用户配置 -> 全局配置

```bash
# 配置用户名
$ git config --global user.name "peterjxl"

# 查看用户名
$ git config user.name

# 删除用户名
$ git config --global --unset user.name


# 编辑Git配置文件
$ git config -e [--global]


# 配置颜色
$ git config --global color.ui true

# 配置别名
$ git config --global alias.st status

# 查看Git配置
$ git config --list
```

　　‍

　　‍

## 增加和删除文件

```bash

# 添加一个文件
$ git add filename

# 添加多个文件
$ git add filename filename2

# 添加指定目录到暂存区，包括子目录
$ git add dirname

# 添加所有文件
$ git add .


# 删除工作区文件，并且将这次删除放入暂存区
$ git rm [file1] [file2] ...


# 停止追踪指定文件，但该文件会保留在工作区
$ git rm --cached [file]
```

　　‍

## 代码提交

　　‍

```bash
# 提交暂存区到仓库区
$ git commit -m [message]


# 提交暂存区的指定文件到仓库区
$ git commit [file1] [file2] ... -m [message]

# 提交暂存区的所有修改，注意Untracked的文件不能用这个来提交
git commit -am [message]

# 提交时显示所有diff信息
$ git commit -v

# 复制一个提交
$ git cherry-pick commit-id
```

　　‍

　　‍

　　‍

## 查看文件差异

```bash
# 查看工作区和暂存区的差异
$ git diff 文件名

# 暂存区和版本库的差别
$ git diff --cached

# 查看工作区和版本库的差别
$ git diff HEAD


# 仅查看被修改的文件名
$ git diff --name-status


# 查看文件的修改记录
$ git blame filename
```

　　‍

## 版本回退和撤销修改

```bash
# 回退到上一版本
$ git reset --hard HEAD^

# 也是回退，不过是复制了一次commit
$ git revert  --hard HEA


# 撤销工作区的某个文件修改
$ git restore <file>

# 撤销工作区的所有修改
$ git restore .


# 撤销添加到暂存区
$ git restore --staged <file>...
```

　　‍

　　‍

## 日志

```bash
# 查看日志
$ git log 

# 查看指定数量的日志
$ git log -n

# 简化日志输出为一行
$ git log --oneline 

# 查看图示
$ git log --graph --abbrev-commit

# 查看输入过的命令
$ git reflog

# 显示commit历史，以及每次commit发生变更的文件
$ git log --stat

# 搜索提交历史，根据关键词
$ git log -S [keyword]

```

　　‍

　　‍

## 分支

　　‍

```bash

# 查看本地分支
$ git branch

# 创建dev分支
$ git branch dev

# 切换分支
$ git switch dev

# 创建并切换分支
$ git switch -c dev


# 切换到上一个分支
$ git switch -

# 删除分支
$ git branch -d dev

# 修改分支名字
$ git branch -m <old_branch_name> <new_branch_name>

# 列出所有远程分支
$ git branch -r

# 查看所有分支（含本地和远程）
$ git branch -a

# 合并分支
$ git merge dev

# 合并分支
$ git merge --no-ff -m "merge with no-ff" feature02


# 将本地分支和远程分支关联
$ git branch --set-upstream-to <branch-name> origin/<branch-name>

# 推送本地分支
$ git push origin dev

# 创建并关联远程分支
$ git switch -c dev origin/dev


```

　　‍

## 远程仓库

　　‍

```bash

# 查看远程仓库信息
$ git remote -v

# 添加远程仓库信息
$ git remote add origin git@github.com:Peter-JXL/LearnGit.git

# 第一次推送到远程库
$ git push -u github master

# 后续推送到远程库
$ git push github

# 拉取更新
$ git pull

# 下载远程仓库的所有变动
$ git fetch [remote]


# 删除远程仓库
$ git remote rm github

# 添加多个推送地址
$ git remote set-url --add 远程仓库名 另一个仓库的push地址

# 删除推送地址
$ git remote set-url --delete origin 地址
```

　　‍

## 标签

```bash
# 查看所有标签
$ git tag

# 查找标签
$ git tag -l "tagname*"

# 为最新的commit 打赏标签
$ git tag tagname

# 为某个commit 打上标签
$ git tag tagname commit-id

# 为某个commit打上带说明的标签
$ git tag -a tagname -m [message] commit-id

# 查看某个标签信息
$ git show tagname


# 删除某个标签
$ git tag -d tagname

# 推送某个标签
$ git push origin tagname

# 推送所有标签
$ git push origin --tags

# 删除远程标签
$ git push origin --delete <tagname>
```

　　‍

　　‍

## stash

```bash

# 保存工作现场
$ git stash

# 保存工作现场并添加说明
$ git stash save "temp"

# 查看所有现场
$ git stash list

# 恢复最新的现场（不删除）
$ git stash apply

# 恢复指定的现场（不删除）
$ git stash apply stash@{0}

# 删除某个现场
$ git stash drop

# 恢复最新的现场（并删除）
$ git stash pop

# 删除所有现场
$ git stash clear

# 查看现场和工作区的差异
$ git stash show
```

　　‍

## 其他Git命令手册

　　[常用 Git 命令清单 - 阮一峰的网络日志](http://www.ruanyifeng.com/blog/2015/12/git-cheat-sheet.html)

　　[Git Cheat Sheet](https://liaoxuefeng.gitee.io/resource.liaoxuefeng.com/git/git-cheat-sheet.pdf)